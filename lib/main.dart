import 'dart:io';
import 'dart:convert';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Kamiko Save Editor'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Map<String, dynamic>? _json = null;

  String decompress(String compressed) {
    var t1 = base64Decode(compressed);
    var t2 = t1.sublist(4);
    var t3 = GZipCodec().decode(t2);
    return utf8.decode(t3);
  }

  String compress(String s) {
    var t1 = utf8.encode(s);
    var len = t1.length;
    var len_buffer = (Uint8List(4)
          ..buffer.asByteData().setInt32(0, len, Endian.little))
        .toList();
    var t2 = GZipCodec().encode(t1);
    var t3 = len_buffer + t2;
    return base64Encode(t3);
  }

  Future<Map<String, dynamic>> load_json(
      String filename, bool compressed) async {
    var file = File(filename);
    var content = await file.readAsString();
    if (compressed) {
      content = decompress(content);
    }
    var json = jsonDecode(content);
    if (compressed) {
      json['MainSaveDataJson'] = jsonDecode(json['MainSaveDataJson']);
      json['SettingSaveDataJson'] = jsonDecode(json['SettingSaveDataJson']);
    }
    return json;
  }

  void save_json(
      String filename, Map<String, dynamic> data, bool compressed) async {
    var t;
    if (compressed) {
      t = {};
      t['MainSaveDataJson'] = jsonEncode(data['MainSaveDataJson']);
      t['SettingSaveDataJson'] = jsonEncode(data['SettingSaveDataJson']);
    } else {
      t = data;
    }
    var s = jsonEncode(t);
    if (compressed) {
      s = compress(s);
    }
    var file = File(filename);
    file.writeAsString(s);
  }

  Future<Directory> get_kamiko_dir() async {
    var appdata = await getApplicationSupportDirectory();
    return Directory(
        "${appdata.parent.parent.parent.path}/LocalLow/Flyhigh works/Kamiko");
  }

  void _load_save_data() async {
    var kamiko = await get_kamiko_dir();
    var json = await load_json('${kamiko.path}/SaveData', true);
    setState(() {
      _json = json;
    });
  }

  void _load_json() async {
    var result = await FilePicker.platform.pickFiles(
      initialDirectory: (await get_kamiko_dir()).path,
      allowedExtensions: ['json'],
    );
    if (result != null) {
      var json = await load_json(result.files.single.path!, false);
      setState(() {
        _json = json;
      });
    }
  }

  void _save_save_data() async {
    var kamiko = await get_kamiko_dir();
    save_json('${kamiko.path}/SaveData', _json!, true);
  }

  void _save_save_data_as() async {
    var kamiko = await get_kamiko_dir();
    var result =
        await FilePicker.platform.saveFile(initialDirectory: kamiko.path);
    if (result != null) {
      save_json(result, _json!, true);
    }
  }

  void _save_json() async {
    var kamiko = await get_kamiko_dir();
    save_json('${kamiko.path}/SaveData.json', _json!, false);
  }

  void _save_json_as() async {
    var kamiko = await get_kamiko_dir();
    var result =
        await FilePicker.platform.saveFile(initialDirectory: kamiko.path);
    if (result != null) {
      save_json(result, _json!, false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
            icon: const Icon(Icons.save_as_outlined),
            onPressed: _save_json_as,
            tooltip: 'save as json file',
          ),
          IconButton(
            icon: const Icon(Icons.save_as),
            onPressed: _save_save_data_as,
            tooltip: 'save as SaveData',
          ),
          IconButton(
            icon: const Icon(Icons.save_outlined),
            onPressed: _save_json,
            tooltip: 'save json file',
          ),
          IconButton(
            icon: const Icon(Icons.save),
            onPressed: _save_save_data,
            tooltip: 'save SaveData',
          ),
          IconButton(
            icon: const Icon(Icons.file_open),
            onPressed: _load_save_data,
            tooltip: 'load SaveData',
          ),
          IconButton(
            icon: const Icon(Icons.file_open_outlined),
            onPressed: _load_json,
            tooltip: 'load json file',
          ),
        ],
      ),
      body: Center(
        child: _json == null
            ? TextButton(
                style: TextButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.blue,
                ),
                onPressed: _load_save_data,
                child: const Text(
                  'Load Save Data',
                ),
              )
            : SingleChildScrollView(
                //child: Container(
                child: SaveData(data: _json!),
                //),
              ),
      ),
    );
  }
}

class SaveData extends StatefulWidget {
  const SaveData({super.key, required this.data});

  final Map<String, dynamic> data;

  @override
  State<SaveData> createState() => _SaveDataState();
}

class _SaveDataState extends State<SaveData> {
  final List<bool> _expanded = [false, false];

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: [
        ExpansionTile(
          title: const Text("MainSaveData"),
          children: [MainSaveData(data: widget.data["MainSaveDataJson"])],
        ),
        ExpansionTile(
          title: const Text("SettingSaveData"),
          children: [SettingSaveData(data: widget.data["SettingSaveDataJson"])],
        ),
      ],
    );
  }
}

class MainSaveData extends StatefulWidget {
  MainSaveData({super.key, required this.data});

  final Map<String, dynamic> data;

  @override
  State<MainSaveData> createState() => _MainSaveDataState();
}

class _MainSaveDataState extends State<MainSaveData> {
  final List expanded = [false, false, false];

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: widget.data['_saveDataUnitList'].map<Widget>((e) {
        return ExpansionTile(
          title: Text(
              "SaveDataUnit ${widget.data['_saveDataUnitList'].indexOf(e)}"),
          leading: Radio<int>(
            value: widget.data['_saveDataUnitList'].indexOf(e),
            groupValue: widget.data['_saveDataNo'],
            onChanged: (value) {
              setState(() {
                widget.data['_saveDataNo'] = value;
              });
            },
          ),
          children: [
            SaveDataUnit(
              data: e,
            )
          ],
        );
      }).toList(),
    );
  }
}

const List<String> CharacterNames = [
  "Yamato",
  "Uzume",
  "Hinome",
];
const List<String> LocationNames = [
  'Forest of Awakening',
  'Sunken Relics',
  'Scorching Labyrinth',
  'Ruins of Yamataikoku',
  'Epilogue',
];

class SaveDataUnit extends StatefulWidget {
  SaveDataUnit({super.key, required this.data});

  final Map<String, dynamic> data;

  @override
  State<SaveDataUnit> createState() => _SaveDataUnitState();
}

class _SaveDataUnitState extends State<SaveDataUnit> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListView(
          shrinkWrap: true,
          children: widget.data['_saveDataUnitEachJobList'].map<Widget>((e) {
            return ExpansionTile(
              title: Text(CharacterNames[
                  widget.data['_saveDataUnitEachJobList'].indexOf(e)]),
              leading: Radio<int>(
                value: widget.data['_saveDataUnitEachJobList'].indexOf(e),
                groupValue: widget.data['_currentJob'],
                onChanged: (value) {
                  setState(() {
                    widget.data['_currentJob'] = value;
                  });
                },
              ),
              children: [
                SaveDataUnitEachJob(data: e),
              ],
            );
          }).toList(),
        ),
        BoolListWidget(
          title: 'Secrets Collected:',
          data: widget.data['IsGotHiddenItemList'],
          labels: LocationNames,
        ),
        BoolListWidget(
          title: 'Special Used:',
          data: widget.data['IsDoneAccumulateAttackActionList'],
          labels: CharacterNames,
        ),
        IntWidget(
          label: 'Enemies Killed',
          value: widget.data['_beatenEnemyCount'],
          onChanged: (value) {
            setState(() {
              widget.data['_beatenEnemyCount'] = value;
            });
          },
        ),
        IntWidget(
          label: 'Objects Broken',
          value: widget.data['_brokenObjectCount'],
          onChanged: (value) {
            setState(() {
              widget.data['_brokenObjectCount'] = value;
            });
          },
        ),
      ],
    );
  }
}

class SaveDataUnitEachJob extends StatelessWidget {
  SaveDataUnitEachJob({super.key, required this.data});

  final Map<String, dynamic> data;

  @override
  Widget build(BuildContext context) {
    return Text("$data");
  }
}

class SettingSaveData extends StatelessWidget {
  const SettingSaveData({super.key, required this.data});

  final Map<String, dynamic> data;

  @override
  Widget build(BuildContext context) {
    return Text("$data");
  }
}

class IntWidget extends StatelessWidget {
  const IntWidget(
      {super.key, required this.label, required this.value, this.onChanged});

  final String label;
  final int value;
  final void Function(int value)? onChanged;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: '$value',
      decoration: InputDecoration(labelText: label),
      keyboardType: TextInputType.number,
      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
      onChanged: (value) {
        var n = int.parse(value);
        onChanged!(n);
      },
    );
  }
}

class BoolListWidget extends StatefulWidget {
  const BoolListWidget({
    super.key,
    required this.title,
    required this.data,
    required this.labels,
  });

  final String title;
  final List<dynamic> data;
  final List<String> labels;

  @override
  State<BoolListWidget> createState() => _BoolListWidgetState();
}

class _BoolListWidgetState extends State<BoolListWidget> {
  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];
    children.add(Text(widget.title));
    children.addAll(widget.data.asMap().entries.map<Widget>((e) {
      var index = e.key;
      var value = e.value;
      return FilterChip(
        label: Text(widget.labels[index]),
        selected: value,
        onSelected: (((value) {
          setState(() {
            widget.data[index] = value;
          });
        })),
      );
    }).toList());
    return Wrap(
      direction: Axis.horizontal,
      children: children,
    );
  }
}
